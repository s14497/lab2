  import java.sql.PreparedStatement;
        import java.sql.Connection;
        import java.sql.Statement;
        import java.sql.ResultSet;
        import java.sql.DriverManager;
        import java.sql.SQLException;
        import java.util.List;
        import java.util.ArrayList;

public class ClientDetailsManager {

    private Connection connection;
    private String url = "...";
    private String createTableClientDetails = "CREATE TABLE clientDetails(id bigint GENERATED BY DEFAULT AS IDENTITY,  name varchar(20), surname varchar(20), login varchar(20))";
    private PreparedStatement addClientDetails;
    private PreparedStatement deleteClientDetails;
    private PreparedStatement getClientDetails;
    private Statement statement;

    public ClientDetailsManager() {
        try {
            connection = DriverManager.getConnection(url);
            statement = connection.createStatement();

            ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
            boolean tableExists = false;
            while (rs.next()) {
                if ("ClientDetails".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
                    tableExists = true;
                    break;
                }
            }

            if (!tableExists)
                statement.executeUpdate(createTableClientDetails);

            	addClientDetails = connection
                    .prepareStatement("INSERT INTO ClientDetails (id, name, surname, login) VALUES (?, ?, ?, ?)");
            	deleteClientDetails = connection
                    .prepareStatement("DELETE FROM ClientDetails");
            	getClientDetails = connection
                    .prepareStatement("SELECT id, name, surname, login FROM ClientDetails");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    Connection getConnection() {
        return connection;
    }

    void clearClientDetails() {
        try {
            deleteClientDetails.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int addClientDetails(ClientDetails details) {
        int count = 0;
        try {
            addClientDetails.setLong(1, details.getId());
            addClientDetails.setString(2, details.getName());
            addClientDetails.setString(3, details.getSurname());
            addClientDetails.setString(4, details.getLogin());

            count = addClientDetails.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public List<ClientDetails> getAddAddress() {
        List<ClientDetails> clientDetails = new ArrayList<ClientDetails>();

        try {
            ResultSet rs = getClientDetails.executeQuery();

            while (rs.next()) {
                ClientDetails a = new ClientDetails();

                a.setId(rs.getLong("id"));
                a.setName(rs.getString("name"));
                a.setSurname(rs.getString("surname"));
                a.setLogin(rs.getString("login"));

                clientDetails.add(a);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clientDetails;
    }
}